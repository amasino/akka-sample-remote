package edu.cbmi

/**
 * Created with IntelliJ IDEA.
 * User: masinoa
 * Date: 3/26/13
 * Time: 9:23 AM
 * To change this template use File | Settings | File Templates.
 */
import akka.kernel.Bootable
import scala.util.Random
//#imports
import com.typesafe.config.ConfigFactory
import akka.actor.{ ActorRef, Props, Actor, ActorSystem }
//#imports

class LookupApplication extends Bootable {
  //#setup
  val system =
    ActorSystem("LookupApplication", ConfigFactory.load.getConfig("remotelookup"))
  val actor = system.actorOf(Props[LookupActor], "lookupActor")
  val remoteActor = system.actorFor(
    "akka://CalculatorApplication@192.168.1.11:2552/user/simpleCalculator")
     //use this if testing on single machine
    //"akka://CalculatorApplication@127.0.0.1:2552/user/simpleCalculator")

  def doSomething(op: MathOp) = {
    actor ! (remoteActor, op)
  }
  //#setup

  def startup() {
  }

  def shutdown() {
    system.shutdown()
  }
}

//#actor
class LookupActor extends Actor {
  def receive = {
    case (actor: ActorRef, op: MathOp) ⇒ actor ! op
    case result: MathResult ⇒ result match {
      case AddResult(n1, n2, r) ⇒
        println("Add result: %d + %d = %d".format(n1, n2, r))
      case SubtractResult(n1, n2, r) ⇒
        println("Sub result: %d - %d = %d".format(n1, n2, r))
    }
  }
}
//#actor

object LookupApp {
  def main(args: Array[String]) {
    val app = new LookupApplication
    println("Started Lookup Application")
    while (true) {
      if (Random.nextInt(100) % 2 == 0)
        app.doSomething(Add(Random.nextInt(100), Random.nextInt(100)))
      else
        app.doSomething(Subtract(Random.nextInt(100), Random.nextInt(100)))

      Thread.sleep(200)
    }
  }
}
