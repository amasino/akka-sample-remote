import sbt._
import sbt.Keys._

object AkkaRemoteSampleBuild extends Build {

  lazy val akkaRemoteSample = Project(
    id = "akka-remote-sample",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "Akka Remote Sample",
      organization := "edu.cbmi",
      version := "0.1-SNAPSHOT",
      scalaVersion := "2.10.0",
      resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases",
      libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.1.2",
      libraryDependencies += "com.typesafe.akka" %% "akka-kernel" % "2.1.2",
      libraryDependencies += "com.typesafe.akka" %% "akka-remote" % "2.1.2"
    )
  )
}
