<h1>Akka-Sample-Remote</h1>
This is a simple Scala Akka project demonstrating the use of remotely deployed Actors and ActorSysytems

It follows the version [here](https://github.com/akka/akka/tree/v2.1.2/akka-samples/akka-sample-remote) but simplified 
to a single project with some bug fixes as noted [here](https://groups.google.com/forum/?fromgroups=#!topic/akka-user/iDjSx9TMlEA).

Requires:
sbt version 0.12.0
scala version 2.10.0
akka-actor version 2.1.2
akka-kernel version 2.1.2
akka-remote version 2.1.2
